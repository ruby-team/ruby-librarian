Source: ruby-librarian
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Stig Sandbeck Mathisen <ssm@debian.org>,
           Sebastien Badia <sbadia@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               git,
               rake,
               ruby-fakefs,
               ruby-rspec,
               ruby-thor (>= 0.15)
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-librarian.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-librarian
Homepage: https://github.com/voxpupuli/librarian
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: binary-targets

Package: ruby-librarian
Architecture: all
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: framework for writing bundlers
 Librarian is a framework for writing bundlers, which are tools that resolve,
 fetch, install, and isolate a project's dependencies, in Ruby.
 .
 A bundler written with Librarian will expect you to provide a specfile listing
 your project's declared dependencies, including any version constraints and
 including the upstream sources for finding them. Librarian can resolve the
 spec, write a lockfile listing the full resolution, fetch the resolved
 dependencies, install them, and isolate them in your project.
 .
 A bundler written with Librarian will be similar in kind to Bundler, the
 bundler for Ruby gems that many modern Rails applications use.
